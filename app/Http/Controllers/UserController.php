<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 21.12.18
 * Time: 17:16
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Rule;

class UserController extends Controller
{

    /**
     * show all Users
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    /**
     * Show User by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = User::find($id);
        if ($user){
            return response()->json($user);
        }else{
            return response()->json(['message'=>'User not found', 'error'=>true],404);
        }

    }

    /**
     * create new User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'login'     => 'required|string|unique:users',
            'password'  => 'required|string',
            'full_name'  => 'string',
            'email'  => 'email|string',
            'phone'  => 'string',
        ]);
        $data = $request->input();
        $data = $this->prepareData($data);
        try {
            $user = User::create($data);
            return response()->json(['status' => 'OK', 'data' => $user], 200);
        }catch(QueryException $e){
            return response()->json(['status'=>'QueryError', 'data'=>[], 'messages'=>$e->errorInfo], 400);
        }
    }

    /**
     * update User by ID
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'password'  => 'string',
            'full_name'  => 'string',
            'email'  => 'email|string',
            'phone'  => 'string',
        ]);
        $data = $request->input();
        $data = $this->prepareData($data, 'PUT');
        try{
            if (User::where('id', $id)->update($data)){
                $user = User::find($id);
                return response()->json(['status'=>'OK', 'data'=>$user], 200);
            }
        }catch(QueryException $e){
            return response()->json(['status'=>'QueryError', 'data'=>[], 'messages'=>$e->errorInfo], 400);
        }
    }

    /**
     * soft deleting User by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try{
            User::where('id', $id)->delete();
            return response()->json(['status'=>'OK', 'data'=>[]], 200);
        }catch(QueryException $e){
            return response()->json(['status'=>'QueryError', 'data'=>[], 'messages'=>$e->errorInfo], 400);
        }

    }

    /**
     * Prepare data to store or update
     * @param $data
     * @param $method
     * @return mixed
     */
    protected function prepareData($data, $method="POST")
    {
        if ($method === 'POST'){
            $data['created_by'] = $data['updated_by'] = Auth::id();
        }elseif ($method==='PUT'){
            $data['updated_by'] = Auth::id();
            if (isset($data['login'])) unset($data['login']);

        }
        if (isset($data['password'])) {
            $data['password'] = app('hash')->make($data['password']);
        }
        return $data;
    }

}