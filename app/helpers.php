<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 21.12.18
 * Time: 15:58
 */

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}