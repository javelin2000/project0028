<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            0 => [
                'id'        => 1,
                'login'     => 'admin',
                'password'  => app('hash')->make('secret'),
                'full_name' => 'Admin Admin',
                'email'     => 'admin@mail.com',
                'phone'     => '+380 55 555 5555',
                'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'updated_at' => (new \DateTime())->format('Y-m-d H:i:s'),
            ]
        ]);
    }
}
