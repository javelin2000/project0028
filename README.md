# project0028

## Installation

1. install dependencies
```bash
    composer install
```
2. create database and add data to .env file to follow fields: DB_DATABASE, DB_USERNAME, DB_PASSWORD

3. Make migrations 
```bash
    artisan migrate
```
4. Make seeds
```bash
    artisan db:seed
```
5. To create APP KEY make request http://your_host/key and copy the resulting string to field APP_KEY in your .env file

6. Create JWT secret key  
```bash
    artisan jwt:secret
```
Project ready to use

## Project routes

Login
Request
    POST
    BODY
        login:string
        password:string
    URL http://host/auth/login
Response
```bash
    JSON {
             "access_token": string,
             "token_type": "bearer",
             "expires_in": integer
         }
```         
Logout         
Request
    POST
    HEADER
        Bearer Token : string
    URL http://host/auth/logout
Response
```bash
    JSON {
             "message": "Successfully logged out"
         }
```         
Current user info
Request
    POST
    HEADER
        Bearer Token : string
    URL http://host/auth/logout
Response
```bash
    JSON {
        "id": integer
        "login": string,
        "full_name": string,
        "email": string,
        "phone": string,
        "created_by": integer,
        "updated_by": integer,
        "created_at": timestamp,
        "updated_at": timestamp,
        "deleted_at": timestamp
    }  
```    
Refresh token        
Request
    POST
   HEADER
        Bearer Token : string
    URL http://host/auth/refresh
Response
```bash
    JSON {
             "access_token": string,
             "token_type": "bearer",
             "expires_in": integer
         }      
```         
Working with user
All routes must have        
   HEADER
        Bearer Token : string
              
get all Users list 
GET http://host/users

get User by ID 
GET http://host/user/{id}

create new use
POST http://host/user/
BODY
    login: string, required
    password: string, required
    full_name: string,
    email: string,
    phone: string,
Response
```bash
    JSON {
             "status": "OK",
             "data": {
                 "login": string,
                 "full_name": string,
                 "email": string,
                 "updated_by": integer,
                 "created_by": integer,
                 "updated_at": timestamp,
                 "created_at": timestamp,
                 "id": integer
             }
         }
```         
Update exist user by ID
PUT http://host/user/{id}
BODY
    password: string, 
    full_name: string,
    email: string,
    phone: string,
Response
```bash
    JSON {
             "status": "OK",
             "data": {
                 "login": string,
                 "full_name": string,
                 "email": string,
                 "updated_by": integer,
                 "created_by": integer,
                 "updated_at": timestamp,
                 "created_at": timestamp,
                 "id": integer
             }
         }
```      
Delete exist user by ID    
DELETE http://host/user/{id}
Response 
```bash
    JSON{
            "status": "OK",
            "data": []
        }
```        